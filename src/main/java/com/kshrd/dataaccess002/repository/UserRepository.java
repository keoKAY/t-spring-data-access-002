package com.kshrd.dataaccess002.repository;

import com.kshrd.dataaccess002.model.Course;
import com.kshrd.dataaccess002.model.Role;
import com.kshrd.dataaccess002.model.User;
import com.kshrd.dataaccess002.model.UserSimplified;
import com.kshrd.dataaccess002.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Map;


@Repository
public interface UserRepository {
    // select_all_users() is a stored procedure in the database server
  /*  @Select(value="SELECT * from select_all_users()")
    List<User> selectAllUsers();
*/




    @Select("select * from students")
    List<User> getAllUser();
    @Select("select * from students where id=#{id}")
    User findUserById(int id);
    @Delete("delete from students where id = #{id}")
    Boolean deleteById(int id);

   @Insert("insert into students (username,gender, bio) values(#{username},#{gender},#{bio})")
  //@Insert("insert into students (username,gender, bio) values(#{user.username},#{user.gender},#{user.bio})")
    Boolean insertUser( User user);

   @Update("update students set username=#{user.username} , gender=#{user.gender},bio=#{user.bio} where id=#{id}")
 //@Update("update students set username=#{username} , gender=#{gender},bio=#{bio} where id=#{id}")
    Boolean updateUser(User user, int id );

   /*@Select("select * from students;")
   @Results(
           {
                   @Result(property = "studentId",column = "id"),
                   @Result(property = "studentName",column = "username"),
                   @Result(property = "studentGender",column = "gender"),
                   @Result(property = "studentBio", column = "bio"),
                  @Result(property = "courseName",column = "id",
                  one=@One(select = "getCourseByID"))
           }
   )
   List<UserSimplified> getAllSimplifiedUser();
   @Select("select course from course where id =(select course_id from students where students.id=#{user_id})")
    String getCourseByID(int user_id);*/


   // MANY

    @Select("select * from students;")
    @Results(
            {
                    @Result(property = "studentId",column = "id"),
                    @Result(property = "studentName",column = "username"),
                    @Result(property = "studentGender",column = "gender"),
                    @Result(property = "studentBio", column = "bio"),
                    @Result(property = "courseName",column = "id",
                            one=@One(select = "getCourseByID")),
                    @Result(property = "roles",column = "id",
                    many = @Many(select = "selectUserRole"))
            }
    )
    List<UserSimplified> getAllSimplifiedUser();


    @Select("select role_name from roles inner join user_role ur on roles.id = ur.role_id where user_id=ur.user_id and user_id=#{user_id}")
    List<String> selectUserRole(int user_id);

    @Select("select course from course where id =(select course_id from students where students.id=#{user_id})")
    String getCourseByID(int user_id);

    @SelectProvider(type = UserProvider.class, method = "getUserByID")
    User getProviderUserById(int id);

    @SelectProvider(type = UserProvider.class, method = "selectUserByWhere")
    List<User> selectUserByWhere(@Param("user")  User user);


}
