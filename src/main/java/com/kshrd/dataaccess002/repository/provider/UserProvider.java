package com.kshrd.dataaccess002.repository.provider;

import com.kshrd.dataaccess002.model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.ObjectUtils;
import org.thymeleaf.util.StringUtils;

public class UserProvider {

    private final String tableName = "students";
    public String getUserByID(int id){

        return new SQL(){{
            SELECT("*");
            FROM("students");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String selectUserByWhere (@Param("user") User user){
        return  new SQL(){{
         SELECT("id","username","gender","bio");
         FROM(tableName);
            if (!ObjectUtils.isEmpty(user)){ // if there is a value of the user object
                if (user.getId() !=0){
                    WHERE("id =#{user.id}");
                }else if (!StringUtils.isEmpty(user.getUsername())){
                    WHERE("username=#{user.username}");
                }else
                {
                    WHERE("1=1"); // always true it's like there is no Where clause
                }
            }
        }}.toString();
    }
}
