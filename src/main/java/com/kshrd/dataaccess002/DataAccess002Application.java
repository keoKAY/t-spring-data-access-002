package com.kshrd.dataaccess002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataAccess002Application {

    public static void main(String[] args) {
        SpringApplication.run(DataAccess002Application.class, args);
    }

}
