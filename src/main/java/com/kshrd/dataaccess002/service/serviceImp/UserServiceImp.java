package com.kshrd.dataaccess002.service.serviceImp;

import com.kshrd.dataaccess002.model.User;
import com.kshrd.dataaccess002.model.UserSimplified;
import com.kshrd.dataaccess002.repository.UserRepository;
import com.kshrd.dataaccess002.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {
    UserRepository userRepository;
    @Autowired
    UserServiceImp(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Override
    public List<User> getAllUser() {
        return userRepository.getAllUser();
    }

    @Override
    public User findUserById(int id) {
        return userRepository.findUserById(id);
    }

    @Override
    public Boolean updateUser(User user, int id ) {
        return userRepository.updateUser(user,id);
    }

    @Override
    public Boolean addNewUser(User user) {
        return userRepository.insertUser(user);
    }

    @Override
    public Boolean deleteUser(int id) {
        return userRepository.deleteById(id);
    }
    @Override
    public List<UserSimplified> getAllSimplifiedUsers() {
        return userRepository.getAllSimplifiedUser();
    }

    @Override
    public User getProviderUserById(int id) {
        return userRepository.getProviderUserById(id);
    }

    @Override
    public List<User> selectUserProvider(User user) {
        return userRepository.selectUserByWhere(user);
    }
}