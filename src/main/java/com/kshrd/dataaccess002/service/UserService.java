package com.kshrd.dataaccess002.service;


import com.kshrd.dataaccess002.model.User;
import com.kshrd.dataaccess002.model.UserSimplified;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;



public interface UserService {

    List<User> getAllUser();
    User findUserById(int id);
    Boolean updateUser (User user, int id);
    Boolean addNewUser(User user);
    Boolean deleteUser ( int id );

    List<UserSimplified> getAllSimplifiedUsers();

//    Provider Operation

    User getProviderUserById(int id);
    List<User> selectUserProvider (User user);

}
