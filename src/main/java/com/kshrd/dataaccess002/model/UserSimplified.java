package com.kshrd.dataaccess002.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class UserSimplified {
private int studentId;
private String studentName;
private String studentGender;
private String studentBio;
private String courseName;
private List<String> roles;

}
