package com.kshrd.dataaccess002.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

@AllArgsConstructor
@NoArgsConstructor
public class Role {
    private int id;
    private String role_name;
}
