package com.kshrd.dataaccess002.model;


import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private int id ;
    private String username ;
    private String gender;
    private String bio;
}
