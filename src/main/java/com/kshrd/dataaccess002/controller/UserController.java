package com.kshrd.dataaccess002.controller;


import com.kshrd.dataaccess002.model.User;
import com.kshrd.dataaccess002.model.UserSimplified;
import com.kshrd.dataaccess002.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {
    UserService userService;

    @Autowired
    UserController(UserService userService){
     this.userService = userService;
    }
    @GetMapping()
    public String index(Model model){
        List<User> allUser = new ArrayList<>();
        try{
         allUser = userService.getAllUser();
            System.out.println("All Users Value : "+ allUser);
        }catch (Exception ex){
            System.out.println("Error SQL : "+ ex.getMessage());
        }

        model.addAttribute("users",allUser);
        return "index";
    }


    @GetMapping("/insert-user")
    public  String insertUser(User user,Model model){
        try{

            user.setUsername("Life Kit");
            user.setGender("M");
            user.setBio("Everyone needs a little help being a human. From sleep to saving money parenting and more, we talk to " +
                    "expert to get the best advice out there. Life Kit is here to help you get it together.");

            Boolean insertUser = userService.addNewUser(user);

            System.out.println("Insert User Status: "+insertUser);


        }catch (Exception ex){


            System.out.println("Error SQL Exception: "+ ex.getMessage());
        }

        model.addAttribute("users",null);
        return "index";
    }
    @GetMapping("/update-user")
    public  String updateUser(User user,Model model){
        try{
 int id=6;

            user.setUsername("Mindset Mentorx");
            user.setGender("M");
            user.setBio("It's time to stop f*cking around and start making progress.");
            Boolean insertUser = userService.updateUser(user,1);

            System.out.println("Update User Status: "+insertUser);


        }catch (Exception ex){


            System.out.println("Error SQL Exception: "+ ex.getMessage());
        }

        model.addAttribute("users",null);
        return "index";
    }
    @GetMapping("/delete-user")
    public  String deleteUser( Model model){
        try{
            int id=6;
       Boolean deleteUser = userService.deleteUser(id);
            System.out.println("deleteStatus User Status: "+deleteUser);

        }catch (Exception ex){
          System.out.println("Error SQL Exception: "+ ex.getMessage());
        }

        model.addAttribute("users",null);
        return "index";
    }
    @GetMapping("/find-by-id")
    public String findUserByID(Model model){

        int id=3;
        List<User> users = new ArrayList<>();
        try{
            User findUser = userService.findUserById(id);
             users.add(findUser);


        }catch (Exception ex){
            System.out.println("Error SQL Exception: "+ ex.getMessage());
        }

        model.addAttribute("users",users);
        return "index";
    }

    @GetMapping("/simplified-users")
    public String simplifiedUsers(Model model){

        List<UserSimplified> simplifiedUser = new ArrayList<>();
        List<User> users = new ArrayList<>();
        try{

            simplifiedUser = userService.getAllSimplifiedUsers();
        }catch (Exception ex){

            System.out.println("Exception Happens : "+ ex.getMessage());
        }
        System.out.println("Simplified Users: ");
        simplifiedUser.stream().forEach(System.out::println);

        model.addAttribute("users",users);
        return "index";
    }


    @GetMapping("/users-provider")
    public String  userProvider(Model model){


        List<User> users = new ArrayList<>();
        try{

            users.add(userService.getProviderUserById(1)) ;
        }catch (Exception ex){

            System.out.println("Exception Happens : "+ ex.getMessage());
        }

        model.addAttribute("users",users);
        return "index";
    }
    @GetMapping("/select-users-provider")
    public String  selectUserProvider(Model model){


        List<User> users = new ArrayList<>();

        User userObj = new User();
      // userObj.setId(2);
       userObj.setUsername("Cher Nich");
        try{
            users = userService.selectUserProvider(userObj);

        }catch (Exception ex){

            System.out.println("Exception Happens : "+ ex.getMessage());
        }

        model.addAttribute("users",users);
        return "index";
    }


}
